package com.example.historical.battles.Service;

import com.example.historical.battles.entity.Battles;
import com.example.historical.battles.repository.BattlesRepositroy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@org.springframework.stereotype.Service
public class Service {
    @Autowired
    BattlesRepositroy battlesRepositroy;

    public Battles getBattles(int id){
        Optional<Battles> battles=battlesRepositroy.findById(id);
        Battles b=battles.orElseThrow(()->new RuntimeException());
        return  b;
    }


}

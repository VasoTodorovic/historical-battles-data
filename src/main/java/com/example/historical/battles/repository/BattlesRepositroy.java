package com.example.historical.battles.repository;

import com.example.historical.battles.entity.Battles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BattlesRepositroy extends JpaRepository<Battles,Integer> {
}

package com.example.historical.battles.entity;

import lombok.*;

import javax.persistence.*;
import javax.persistence.Id;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class BattleActors {
    @Id
    private int Id;
    @ManyToOne
    @JoinColumn(name="isqno")
    private Battles isqno;

    private int atp_number;

    private int start_time_min;

    private int start_time_max;

    private int end_time_min;

    private int end_time_max;

    private int duration_max;

    private int duration_min;

    private int duration_onl;

}


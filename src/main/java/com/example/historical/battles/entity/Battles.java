package com.example.historical.battles.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Battles {

    @Id
    private int isqno;
    private String war;
    private String name;
    private String locn;
    private String campgn;
    private int postype;
    private String post1;
    private String post2;
    private int front;
    private int depth;
    private int time;
    private int aeroa;
    private String surpa;
    private String cea;
    private String leada;
    private int  trnga;
    private int morala;
    private int logsa;
    private String momnta;
    private String intela;
    private int techa;
    private String inita;
    private String wina;
    private String kmda;
    private int crit;
    private String quala;
    private String resa;
    private int mobila;
    private int aira;
    private String fprepa;
    private int wxa;
    private String terra;
    private String leadaa;
    private String plana;
    private String surpaa;
    private int mana;
    private int logsaa;
    private String fortsa;
    private int deepa;
    private int Is_Hero;
    private String War2;
    private String War3;
    private String War4;
    private String Dbpedia;
    private String CowWarno;
    private String CowWarname;
    private int WarInitiator;
    private String Parent;
    private String war4_theater;

    @OneToOne(fetch =FetchType.EAGER,mappedBy = "isqno")
    BattleDurations battleDurations;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "isqno")
    List<BattleActors> battleActors;

}

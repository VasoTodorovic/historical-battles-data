package com.example.historical.battles.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class BattleDurations {
    @Id
    private int id;

    @OneToOne
    @JoinColumn(name = "isqno")
    private Battles isqno;

    private String datetime_min;
    private String datetime_max;
    private String datetime;
    private int duration1;
    private int duration2;
}

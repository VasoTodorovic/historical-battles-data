package com.example.historical.battles;

import com.example.historical.battles.Service.Service;
import com.example.historical.battles.entity.BattleActors;
import com.example.historical.battles.entity.BattleDurations;
import com.example.historical.battles.entity.Battles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class HistoricalBattlesApplication implements CommandLineRunner {
@Autowired
Service service;
	public static void main(String[] args) {
		SpringApplication.run(HistoricalBattlesApplication.class, args);
	}
	@Override
	public void run(String... args) {
		Battles battles= service.getBattles(659);
		BattleDurations battleDurations=battles.getBattleDurations();
		List<BattleActors> battleActors=battles.getBattleActors();
		System.out.println(battleDurations.getDuration1());

		for(BattleActors battleActor:battleActors){

			System.out.println(battleActor.getDuration_max());		}

	}

}
